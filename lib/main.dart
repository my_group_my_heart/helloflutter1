import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _myStatefulwidgetState createState() => _myStatefulwidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String ThaiGreeting = "Thai Flutter";
String JapenGreetion = "Japen Flutter";

class _myStatefulwidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,

        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting ?
                      spanishGreeting : englishGreeting;


                    });
                  },
                  icon: Icon(Icons.refresh)),
              IconButton(onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting ?
                  ThaiGreeting : englishGreeting;


                });
              },
                  icon: Icon(Icons.account_box)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting ?
                      JapenGreetion : englishGreeting;

                    });
                  },
                  icon: Icon(Icons.feed)),
            ],
          ),
          body: Center(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 24),
            ),
          ),
        ));
  }
}
//
// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           appBar: AppBar(
//             title: Text("Hello Flutter"),
//             leading: Icon(Icons.home),
//             actions: <Widget>[
//               IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
//             ],
//           ),
//           body: Center(
//             child: Text(
//               "Hello Flutter",
//               style: TextStyle(fontSize: 24),
//             ),
//           ),
//         ));
//   }
// }
